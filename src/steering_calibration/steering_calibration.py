#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32, String

rospy.init_node("steering_calibration")
pub = rospy.Publisher("/steering_calibration", String, queue_size=20)

def callback(msg):
    print("Requesting data from LaserScan ...")
    """Callback for the ROS subscriber."""
    val = Float32(msg)
    pub.pulish(String('Recieved message %s: '% (msg, val)))
    print('Recieved message %s: '% msg)


sub = rospy.Subscriber("/scan/LaserScan", Float32, callback=callback)
rospy.spin()