#!/usr/bin/env python2

import sys

damnVals = [0.3967852340780831, 0.28655792116953055, 0.13636611612784297, -0.01, -0.139860631021484, -0.2675472287070919, -0.41853095368099036]
damnSteering = [0, 30, 60, 90, 120, 150, 180]

arg = float(sys.argv[1])

def mapping_function(arg):
    for i in range(len(damnVals)-1):
        a = damnVals[i]
        b = damnVals[i+1]
        if arg <= a and arg >= b:
            c = (arg - a) / (b - a)
            return(damnSteering[i] + (abs(c) * 30))


print(mapping_function(arg))