#!/usr/bin/env python

# circumference: 19.47787
# imports

import cv2
import rospy
import sys
import time
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from std_msgs.msg import Int16, UInt8
import signal

# class

class follow_line:

    def __init__(self):
    
        self.speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=20)
        self.steering_pub = rospy.Publisher("/steering", UInt8, queue_size=20)
    
        self.image_pub = rospy.Publisher("follow_line/follow_line_vision", Image, queue_size=20)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/image_processing/bin_img", Image, self.callback, queue_size=20)
        self.image_raw_sub = rospy.Publisher("/camera/color/image_raw", Image, self.callback, queue_size=20)

        self.top = None
        self.bottom = None

        self.offset_angle = 0
        self.timestamp_steering_offset = None

    def callback(self, data):

        angle = 0
        speed = 0

        print("Loading binary image")
        try:
            color_img = self.bridge.imgmsg_to_cv2(data, "bgr8")
            cv_image_binary = self.bridge.imgmsg_to_cv2(data, "mono8")

        except CvBridgeError as e:
            print(e)

    # commands

    # 1. cut image to get relevant data

        print("Cutting image")
        np_array = np.asarray(cv_image_binary)
        #print (np_array.shape) # Original image resolution: ((y=480),(x=640))

        resized_img = cv_image_binary[160:420, 50:590]
        resized_color_img = color_img[160:420, 50:590]
        np_array = np.asarray(resized_img)
        #print(np_array.shape) # Resized image resolution: ((y=270),(x=540))

    # 2. Detect lines using hough algorithm

        contours = cv2.findContours(resized_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)[1]
        #print(type(contours))
        print("Found %s clusters." % len(contours))
        #print(contours)

        found_path = 0
        for i in contours:
            if len(i)>200:
                for a in i:
                    #print(a[0])
                    if 25 <=(a[0][0])<= 515 and 270>=(a[0][1])>= 240:
                        #print("success!", a[0])
                        #print(i)
                        contours = i
                        print("Major cluster has %s pixels." % len(contours))
                        found_path = 1
                        break

        if found_path == 1:
            contour_image = cv2.drawContours(resized_color_img, contours, -1, (170, 240, ), 5)
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(contour_image, "bgr8"))
            
        #resized_img = contour_image

        

    # 3. calculate steering angle

        def calculate_steering_angle(contours):
            print(type(contours))
            try:
                contours = contours.tolist()
            except:
                print("Didn't found contours.")
                return
            #print(type(contours))
            #print(contours)
            top_coordinate = [270, 540]
            bottom_coordinate = [270, 0]
            for i in contours:
                if i[0][1]<(top_coordinate[0]):
                   top_coordinate = i[0]
                if i[0][1]>bottom_coordinate[1]:
                    bottom_coordinate = i[0]

            middle_curve_height = (bottom_coordinate[0]-top_coordinate[0]//3) 

            middle_coordinate = [135, 0]
            if top_coordinate != [270, 540] and bottom_coordinate != [270, 0]:

                for i in contours:
                    if i[0][1] == middle_curve_height:
                        middle_coordinate = i[0]
                
                bot = (bottom_coordinate[0]-270)*(1.0/2.0)*(3.0/16.0)
                mid = (middle_coordinate[0]-270)*(1.0/2.0)*(13.0/16.0)
                angle = int(90+bot+mid)
                #print("middle_curve_height = %s" % middle_curve_height)
                print("Top coordinate = %s" % top_coordinate)
                print("Middle coordinate = %s" % middle_coordinate)
                print("Bottom coordinate = %s" % bottom_coordinate) 
                print("Resulting angle = %s" % angle)
                print(bot, mid)
            
                return angle
            else:
                print("No line found ...")
                return 
            
    
    # 4. publish steering angle and fixed speed

        angle = calculate_steering_angle(contours)
        speed = 160
        self.drive(speed, angle)

    

    def drive(self,speed, angle):
        if angle == None:
            angle = 90
        print("Setting Speed %s and Angle %s" % (speed, angle))
        self.speed_pub.publish(speed)
        self.steering_pub.publish(angle)
        #print("Offset_angle: %s" % self.offset_angle)



def main(args):
    rospy.init_node('follow_line', anonymous=True)
    fl = follow_line()

    def rescue(*_):
        fl.drive(0, 90)
        rospy.signal_shutdown("Bye")

    signal.signal(signal.SIGINT, rescue)

    rospy.spin()

    cv2.destroyAllWindows

if __name__ == '__main__':
    main(sys.argv)
