#!/usr/bin/env python
import roslib
import random
# roslib.load_manifest('my_package')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
#import matplotlib
#matplotlib.use('Agg')
from matplotlib import pyplot as plt

# from __future__ import print_function

class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("/image_processing/bin_img",Image, queue_size=20)
    self.image_pub_gray = rospy.Publisher("/image_processing/gray_img",Image, queue_size=20)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/color/image_raw",Image,self.callback, queue_size=20)


  def callback(self,data):
    print('loading image data')
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)


    #make it gray
    print('converting to gray scale')
    gray=cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)

    print('publishing gray image')
    try:
      self.image_pub_gray.publish(self.bridge.cv2_to_imgmsg(gray, "mono8"))
    except CvBridgeError as e:
      print(e)

    #bi_gray
    print('converting to binary')
    bi_gray_max = 255
    bi_gray_min = 245
    ret,thresh1=cv2.threshold(gray, bi_gray_min, bi_gray_max, cv2.THRESH_BINARY);

    """

    # find dots
    print('eroding and diluting image')
    thresh1 = cv2.erode(thresh1, None, iterations=1)
    thresh1 = cv2.dilate(thresh1, None, iterations=2)

    

    print('publishing binary image')
    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(thresh1, "mono8"))
    except CvBridgeError as e:
      print(e)

    print('done')

    """

    #gauss
    MAX_KERNEL_LENGTH = 2;
    i= 5
    dst=cv2.GaussianBlur(cv_image,(5,5),0,0)

    #edge
    dx = 1;
    dy = 1;
    ksize = 3; #1,3,5,7
    scale = 1
    delta = 0
    edge_img=cv2.Sobel(thresh1, cv2.CV_8UC1, dx, dy, ksize, scale, delta, cv2.BORDER_DEFAULT)

    #bi_rgb
    r_max = 244;
    r_min = 0;
    g_max = 255;
    g_min = 0;
    b_max = 255;
    b_min = 0;
    b,g,r = cv2.split(cv_image)
    for j in range(cv_image.shape[0]):
      for i in range(cv_image.shape[1]):
        if (r[j,i] >= r_min and r[j,i] <= r_max):
          if (g[j,i] >= g_min and g[j,i] <= g_max):
            if (b[j,i] >= b_min and b[j,i] <= b_max):
              r[j,i]=0
              g[j,i]=0
              b[j,i]=0
            else:
              r[j,i]=255
              g[j,i]=255
              b[j,i]=255
    bi_rgb = cv2.merge((b,g,r))

    #bi_hsv
    h_max = 255;
    h_min = 0;
    s_max = 255;
    s_min= 0;
    v_max = 252;
    v_min = 0;
    hsv=cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV);
    h,s,v = cv2.split(hsv)

    for j in xrange(hsv.shape[0]):
      for i in xrange(hsv.shape[1]):
        if  (v[j,i]>= v_min and v[j,i]<= v_max and s[j,i]>= s_min and s[j,i]<= s_max and h[j,i]>= h_min and h[j,i]<= h_max):
          h[j,i]=0
          s[j,i]=0
          v[j,i]=0
        else:
          h[j,i]=255
          s[j,i]=255
          v[j,i]=255

    bi_hsv = cv2.merge((h,s,v))

    # titles = ['Original Image', 'GRAY','BINARY','GAUSS','EDGE','BI_RGB','BI_HSV']
    # images = [cv_image, gray, thresh1,dst,edge_img,bi_rgb,bi_hsv]
    #
    # for i in xrange(7):
    #   plt.subplot(2,4,i+1),plt.imshow(images[i],'gray')
    #   plt.title(titles[i])
    #   plt.xticks([]),plt.yticks([])
    #
    # plt.show()
    # print("Done")

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(thresh1, "mono8"))
    except CvBridgeError as e:
      print(e)

    
    # BlobDetection


    detector = cv2.SimpleBlobDetector_create()

    keypoints = detector.detect(hsv)

    print("")
    print("Amount of Points detected:", len(keypoints))
    print("Coordinates of detected points:")
    print("")

    imageBlobs = []
    for i in range(len(keypoints)):
      cordX = keypoints[i].pt[0]
      cordY = keypoints[i].pt[1]
      imageBlobs.append((cordX, cordY))

      print(cordX, cordY)
    print(imageBlobs)
  
    print("")  

    """

    # SolvePnP and Rodrigues (unfinished)



    fx = 614.1699
    fy = 614.9002
    cx = 329.9491
    cy = 237.2788
    camera_mat = np.zeros((3,3,1))
    camera_mat[:,:,0] = np.array([[fx, 0, cx],
    [0, fy, cy],
    [0, 0, 1]])
    k1 = 0.1115
    k2 = -0.1089
    p1 = 0
    p2 = 0
    dist_coeffs = np.zeros((4,1))
    dist_coeffs[:,0] = np.array([[k1, k2, p1, p2]])
    # far to close, left to right (order of discovery) in cm
    obj_points = np.zeros((6,3,1))
    obj_points[:,:,0] = np.array([[00.0, 00.0, 0],
    [21.8, 00.0, 0],
    [00.0, 30.0, 0],
    [22.2, 30.0, 0],
    [00.0, 60.0, 0],
    [22.0, 60.0, 0]])

    #image_points = np.random.random((10,2,1)) # Testvalues for solvePnP

    # Debugging
    print (obj_points)
    print (image_points)
    print (camera_mat)
    print(dist_coeffs)
    

    #retval, rvec, tvec = cv2.solvePnP(obj_points, image_points, camera_mat, dist_coeffs) # Crash ... Assertion failed
    rmat = np.zeros((3,3))

    rvec_test = np.zeros((1,3))

    rodrigues_result = cv2.Rodrigues(rvec_test, rmat, jacobian=0)
    print ("rodrigues_result (testvalues):")
    print (rodrigues_result)

    #print("retval: ", retval)
    print("rvec: ", rvec_test)
    #print("tvec: ", tvec)
    



# parameters to alter the default ones from SimpleBlobDetector


    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()
    
    # Change thresholds
    params.minThreshold = 10;
    params.maxThreshold = 200;
    
    # Filter by Area.
    params.filterByArea = True
    params.minArea = 1500
    
    # Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.1
    
    # Filter by Convexity
    params.filterByConvexity = True
    params.minConvexity = 0.87
    
    # Filter by Inertia
    params.filterByInertia = True
    params.minInertiaRatio = 0.01
    
    # Create a detector with the parameters
    ver = (cv2.__version__).split('.')
    if int(ver[0]) < 3 :
        detector = cv2.SimpleBlobDetector(params)
    else : 
        detector = cv2.SimpleBlobDetector_create(params)
        


"""
    

  

def main(args):
  rospy.init_node('image_converter', anonymous=True)
  ic = image_converter()
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
