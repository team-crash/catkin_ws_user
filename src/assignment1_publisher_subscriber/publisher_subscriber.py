#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32, String

rospy.init_node('assignment1_publisher_subscriber')

pub = rospy.Publisher('/assignment1', String)

def callback(msg):
    """Callback for the ROS subscriber."""
    val = Float32(msg)
    pub.publish(String('Received message %s, decoded into %s' % (msg, val)))
    print('Received message %s, decoded into %s' % (msg, val))

sub = rospy.Subscriber('/yaw', Float32, callback=callback)
rospy.spin()
