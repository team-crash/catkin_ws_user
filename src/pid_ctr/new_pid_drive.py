#!/usr/bin/env python

# circumference: 19.47787
# 1m = 178 ticks
# /speed vermutlich mm/s
# imports

import rospy
import sys

import time
import numpy as np
import matplotlib.pyplot as plt
from sensor_msgs.msg import Image
from std_msgs.msg import Int16, UInt8, Float32
import signal
from scipy.interpolate import BSpline, make_interp_spline


WHEEL_CIRCUMFENCE = 19.47787
TICKS_PER_METER = 178.0


# class
class pid_ctr:

    def __init__(self, p, i, d):
        self.rpm = 0 # Output value
        self.output = 0 # outp rpm
        self.aimed_velocity = 0.1 #m/s
        self.current_speed = 0 
        self.P = p
        self.I = i
        self.D = d
        self.current_ticks = 0
        self.last_time = time.time()
        self.start_time = None
        
        self.target_hist = []
        self.current_hist = []
        self.time_hist = []
        self.speed_hist = []

        self.previous_error = 0
        self.integral = 0 

        self.pulse_sensor_sub = None
        self.speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=20)
    

    def drive_speed(self, speed):
        """Calculate the ticks and speed required to reach distance (m) in duration (s)."""

        # reset the values
        self.last_time = time.time()
        self.start_time = self.last_time

        # calculate the ticks required to drive the distance and the initial speed to drive the distance with the duration.
        meters_per_second = speed / 3.6
        self.target_ticks = meters_per_second * TICKS_PER_METER

        # publish the initial speed
        self.speed_pub.publish(meters_per_second * 1000.0)

        # initialize the callback
        self.pulse_sensor_sub = rospy.Subscriber("/ticks", UInt8, self.callback, queue_size=20)


    def callback(self, data):
        now = time.time()
        self.time_hist.append(now)
        ticks = int(data.data)

        # calculate where we should be
        self.current_ticks = ticks / (now - self.last_time)
        self.current_hist.append(self.current_ticks)
        self.target_hist.append(self.target_ticks)

        print("current ticks/s: %s; set ticks/s: %s" % (self.current_ticks, self.target_ticks))
        v = self.pid_regler(self.target_ticks, self.current_ticks, now - self.last_time)
        print("Calculated ticks: %s" % v)
        self.speed_hist.append(v)

        self.last_time = now
        self.speed_pub.publish(v + 110)


    def pid_regler(self, setpoint, messured_value, delta):
        error = setpoint - messured_value
        self.integral = self.integral + error * delta
        derivative = (error - self.previous_error) / delta
        result = self.P * error + self.I * self.integral + self.D * derivative
        self.previous_error = error

        return result

def ticks2dist(ticks):
    """Convert ticks into meters."""
    return ticks / TICKS_PER_METER

def dist2ticks(dist):
    """Convert meters into ticks."""
    return dist * TICKS_PER_METER

def rot2dist(rot):
    """Convert rotations to meters."""
    return rot * (WHEEL_CIRCUMFENCE / 100.0)

def dist2rot(dist):
    """Convert distance into rotations."""
    return dist * (100.0 / WHEEL_CIRCUMFENCE)

def drive():
    pass


def main(args):
    rospy.init_node('pid_ctr', anonymous=True)
    pidctr = pid_ctr(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]))

    pidctr.drive_speed(1.0)

    def rescue(*_):
        print("Plotting")

        plt.plot(pidctr.target_hist, pidctr.current_hist, pidctr.speed_hist)
        plt.xlabel('time (10ms)')
        plt.ylabel('speed (RPM)')
        plt.show()

        rospy.signal_shutdown("Bye")

    signal.signal(signal.SIGINT, rescue)

    rospy.spin()
    

if __name__ == '__main__':
    main(sys.argv)