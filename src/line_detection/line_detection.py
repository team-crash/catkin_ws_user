#!/usr/bin/env python

import sys
import time

import numpy as np
import cv2
from matplotlib import pyplot as plt

import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image


MIN_MATCH_COUNT = 6         # Specify amountof points
STALE_IMAGE_TIMEOUT = 10     # Wait sec until old img discard


class line_detection:

  def __init__(self):
    self.image_pub = rospy.Publisher("/line_detection/line_plot_img",Image, queue_size=20)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/image_processing/bin_img",Image,self.callback, queue_size=20)
    self._last_image = None
    self._last_timestamp = 0

  def last_image(self, replace=None):
    """Read the last image if it is not too old and replace it with the replacement image."""
    now = time.time()
    buf = self._last_image
    if self._last_timestamp < now - STALE_IMAGE_TIMEOUT:
        buf = None
    self._last_image = replace
    self._last_timestamp = now

    return buf

  def callback(self,data):
    print("Trying to publish image ...")
    try:
      bin_img = self.bridge.imgmsg_to_cv2(data, "mono8")
    except CvBridgeError as e:
      print(e)

    bin_img2 = self.last_image(replace=bin_img) # trainImage
    if bin_img2 is None:
        # There is no older image data to work with
        return

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create(20) # API change in version 3

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(bin_img,None)
    kp2, des2 = sift.detectAndCompute(bin_img2,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1,des2,k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    # --

    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)              # RANSAC
        matchesMask = mask.ravel().tolist()

        h,w = bin_img.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)

        bin_img2 = cv2.polylines(bin_img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)

    else:
        print("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))

    # --

    draw_params = dict(matchColor = (255,0,0), # draw matches in red color
                    singlePointColor = None,
                    matchesMask = matchesMask, # draw only inliers
                    flags = 2)

    image_pub_lines = cv2.drawMatches(bin_img,kp1,bin_img2,kp2,good,None,**draw_params)

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(image_pub_lines, "bgr8"))
    except CvBridgeError as e:
      print(e)    

    # plt.imshow(img3, 'gray'),plt.show()


# --

def main(args):
  rospy.init_node('line_detection', anonymous=True)
  ld = line_detection()
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)

