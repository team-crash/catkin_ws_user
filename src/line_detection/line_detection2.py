#!/usr/bin/env python
import numpy as np
import cv2
from matplotlib import pyplot as plt
import rospy 
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import sys
import time

print("Hello World!")
MIN_MATCH_COUNT = 6         # Specify amount of points
STALE_IMAGE_TIMEOUT = 10     # Wait sec until old img discard

MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.15

class line_detection:

  def __init__(self):
    print("Initialize node ...")
    self.image_pub = rospy.Publisher("/line_detection/line_plot_img",Image, queue_size=20)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/image_processing/bin_img",Image,self.callback, queue_size=20)
    self._last_image = None
    self._last_timestamp = 0

  def last_image(self, replace=None):
    """Read the last image if it is not too old and replace it with the replacement image."""
    now = time.time()
    buf = self._last_image
    if self._last_timestamp < now - STALE_IMAGE_TIMEOUT:
        buf = None
    self._last_image = replace
    self._last_timestamp = now

    return buf

 
  # WORK TO DO BELOW
  def alignImages(self, im1, im2):
  
    # Convert images to grayscale
    im1Gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2Gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
    
    # Detect ORB features and compute descriptors.
    orb = cv2.ORB_create(MAX_FEATURES)
    keypoints1, descriptors1 = orb.detectAndCompute(im1Gray, None)
    keypoints2, descriptors2 = orb.detectAndCompute(im2Gray, None)
    
    # Match features.
    matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(descriptors1, descriptors2, None)
    
    # Sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)
  
    # Remove not so good matches
    numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:numGoodMatches]
  
    # Draw top matches
    imMatches = cv2.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
    cv2.imwrite("matches.jpg", imMatches)
    
    # Extract location of good matches
    points1 = np.zeros((len(matches), 2), dtype=np.float32)
    points2 = np.zeros((len(matches), 2), dtype=np.float32)
  
    for i, match in enumerate(matches):
      points1[i, :] = keypoints1[match.queryIdx].pt
      points2[i, :] = keypoints2[match.trainIdx].pt

    h = cv2.findHomography(points1, points2, cv2.RANSAC)
    
    print(h)
    return  h

    # WORK TO DO ABOVE

  def callback(self,data):
    print("Trying to publish image ...")
    try:
      bin_img = self.bridge.imgmsg_to_cv2(data, "mono8")
    except CvBridgeError as e:
      print(e)

    bin_img2 = self.last_image(replace=bin_img) # trainImage
    if bin_img2 is None:
        # There is no older image data to work with
        return

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(image_pub_lines, "bgr8"))
    except CvBridgeError as e:
      print(e)    

    # plt.imshow(img3, 'gray'),plt.show()


# --

def main(args):
  rospy.init_node('line_detection', anonymous=True)
  ld = line_detection()
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)

