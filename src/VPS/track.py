
import math
import matplotlib.pyplot as plt
import Queue


SECTOR_TOP = 'top'
SECTOR_BOTTOM = 'bottom'
SECTOR_LEFT = 'left'
SECTOR_RIGHT = 'right'

LASER_SCANNER_OFFSET = 20.0
MAX_BLOCKER_DISTANCE_FROM_TRACK = 10.0
MAX_LOOKAHEAD_DISTANCE = 150.0


q = Queue.Queue()


class Track:
    """Manage our track data."""

    def __init__(self, center_top, center_bottom, radius):
        """Initialize the track data."""
        # store the parameters for later use
        self._center_top = center_top
        self._center_bottom = center_bottom
        self._radius = radius

        # calculate the remaining data
        self._left = self._center_top[0] - self._radius
        self._right = self._center_top[0] + self._radius

        # calculate the length of the individual parts
        self._half_circle_length = math.pi * self._radius
        self._straight_length = self._center_bottom[1] - self._center_top[1]
        self._circumfence = 2 * (self._half_circle_length + self._straight_length)

        # calculate the start point (center of the left straight)
        self._start = (self._left, self._center_top[1] + self._straight_length/2)

    def get_progress(self, position):
        """Calculate the progress the car has made on its way around the track."""
        sector = self.get_sector(position)
        if sector == SECTOR_LEFT:
            if position[1] < self._start[1]:
                # the car is behind the start
                return self._start[1] - position[1]
            
            # the car is before the start
            return self._center_bottom[1] - position[1] + 2*self._half_circle_length + 1.5*self._straight_length

        if sector == SECTOR_RIGHT:
            return position[1] - self._center_top[1] + self._half_circle_length + 0.5*self._straight_length

        if sector == SECTOR_TOP:
            return self._top_progress(position) + 0.5*self._straight_length

        return self._bottom_progress(position) + self._half_circle_length + 1.5*self._straight_length

    def _top_progress(self, position):
        """Calculate the progress on the top half circle from a position."""
        vec = vec_sub(position, self._center_top)
        norm = vec_norm(vec)

        deg = math.acos(norm[0])
        inv_prog = (deg / math.pi) * self._half_circle_length

        return self._half_circle_length - inv_prog

    def _bottom_progress(self, position):
        """Calculate the progress on the bottom half circle from a position."""
        vec = vec_sub(position, self._center_bottom)
        norm = vec_norm(vec)

        # inverting the vector for the bottom half
        norm = vec_mul(norm, -1.0)

        deg = math.acos(norm[0])
        inv_prog = (deg / math.pi) * self._half_circle_length

        return self._half_circle_length - inv_prog


    def get_position(self, progress):
        """Calculate the position the car would have if it had made it to this progress."""
        progress = progress % self._circumfence

        if progress < 0.5*self._straight_length:
            # still on the first strait
            return (self._left, self._start[1] - progress)

        if progress < 0.5*self._straight_length + self._half_circle_length:
            # in the first (top) half circle
            return self._top_position(progress - 0.5*self._straight_length)

        if progress < 1.5*self._straight_length + self._half_circle_length:
            # on the second strait
            p = progress - (0.5*self._straight_length + self._half_circle_length)
            return (self._right, self._center_top[1] + p)

        if progress < 1.5*self._straight_length + 2*self._half_circle_length:
            # on the last (bottom) half circle
            return self._bottom_position(progress - (1.5*self._straight_length + self._half_circle_length))

        # on the last straight (before the start)
        p = progress - (1.5*self._straight_length + 2*self._half_circle_length)
        return (self._left, self._center_bottom[1] - p)


    def distance_on_track(self, a, b):
        """Calculate the distance on the track between two progresses."""
        if a == b:
            return 0

        if a < b:
            return b - a

        return self._circumfence - (b - a)


    def _top_position(self, progress):
        """Calculate the position in the top half circle."""
        inv_prog = self._half_circle_length - progress
        arc = (inv_prog / self._half_circle_length) * math.pi
        x = math.cos(arc)
        y = -math.sin(arc)
        vec = vec_mul((x, y), self._radius)

        return vec_add(vec, self._center_top)


    def _bottom_position(self, progress):
        """Calculate the position in the bottom half circle."""
        inv_prog = self._half_circle_length - progress
        arc = (inv_prog / self._half_circle_length) * math.pi
        x = math.cos(arc)
        y = -math.sin(arc)
        vec = vec_mul((x, y), self._radius)

        # invert the vector for the bottom half circle
        vec = vec_mul(vec, -1.0)

        return vec_add(vec, self._center_bottom)


    def get_sector(self, position):
        """Calculate which sector the car is in."""
        if position[1] <= self._center_top[1]:
            return SECTOR_TOP

        if position[1] >= self._center_bottom[1]:
            return SECTOR_BOTTOM

        if position[0] <= self._center_top[0]:
            return SECTOR_LEFT

        return SECTOR_RIGHT


# VECTOR FUNCTIONS

def vec_len(v):
    """Calculate the length of a vector."""
    return math.sqrt(v[0]**2 + v[1]**2)

def vec_add(v1, v2):
    """Adds two vectors."""
    return (v1[0] + v2[0], v1[1] + v2[1])

def vec_sub(v1, v2):
    """Subtracts two vectors."""
    return (v1[0] - v2[0], v1[1] - v2[1])

def vec_mul(v, f):
    """Multiplies a vector."""
    return (v[0] * f, v[1] * f)

def vec_div(v, d):
    """Divides a vector."""
    return (v[0] / d, v[1] / d)

def vec_norm(v):
    """Normalizes a vector."""
    return vec_div(v, vec_len(v))

def vec_scal(v1, v2):
    """Calculate the scalar from two vectors."""
    return v1[0]*v2[0] + v1[1]*v2[1]

def vec_dist(v1, v2):
    """Calculate the distance between two points."""
    return vec_len(vec_sub(v1, v2))

def deg_to_vec(deg):
    """Transform the degrees value to a norm vector."""
    rad = math.radians(deg)
    return (math.cos(rad), math.sin(rad))

def vec_to_deg(vec):
    """Transform the vector to a degrees value."""
    return math.degrees(math.atan2(vec[1], vec[0]))

def clamp(val, min, max):
    """Make sure val is in the interval (min, max)."""
    if val < min:
        return min
    if val > max:
        return max

    return val

# DOT CLOUD FUNCTIONS

def calculate_absolute_coordinates(position, direction, distances): # done
    """Calculate the absolute coodinates for the laser scanner."""
    direction = vec_norm(direction)[::-1]                    # make sure the direction vector is normalized
    offset = vec_mul(direction, LASER_SCANNER_OFFSET)  # calculate the offset to be applied to the coordinates

    results = []
    xs = []
    ys = []

    for i, d in enumerate(distances):
        d *= 100
        if math.isinf(d):
            continue  # the coordinate is infinite, nothing to see here

        angle = -(((float(i) + 180.0) % 360.0) - 180.0)                             # calculate the angle in vehicle coordinates

        abs_angle = (angle + vec_to_deg(direction)) % 360.0  # calculate the angle in world coordinates
        point = vec_mul(deg_to_vec(abs_angle), d)            # calculate the point in local coordinates
        point = vec_add(point, position)                     # calculate the point in world coordinates
        point = vec_add(point, offset)                       # calculate the point in world coordinates relative to the car (and not the laser scanner)

        #print ("Writing %s to coords.txt." % point)
        #out.write("%s, %s\n" % point)

        # print("Found valid point for angle %s: %s (distance: %s)" % (angle, str(point), d))
        # out.write("%s, %s\n" % point)

        results.append(point)                                # append the result to the result list
        xs.append(point[0])
        ys.append(point[1])

        # print("Laser scan has detected %s obstacles." % len(results))

    ahead = vec_add(vec_mul(direction, 20.0), position)
    q.put((xs, ys))
    q.put(([position[0], ahead[0]], [position[1], ahead[1]]))

    return results


def calculate_closest_progs(track, positions, max_distance):
    """Calculate the progess of points near the track."""

    results = []
    coords = []

    for p in positions:
        prog = track.get_progress(p)  # Calculate the progress on the track
        c = track.get_position(prog)  # Calculate the position on the track
        l = vec_dist(p, c)            # Calculate the distance between the blocker and the track

        if l <= max_distance:         # Filter the potential blockers that are too far from the track
            results.append(prog)
            coords.append(p)

    return results, coords


def check_lane_blocked(track, position, blockers, coords, lookahead):
    """Check the current position against blockers on the track."""
    prog = track.get_progress(position)

    p = track.get_position(prog)

    q.put(([p[0]], [p[1]]))

    for i, b in enumerate(blockers):
        if 15.0 <= track.distance_on_track(b, prog) <= lookahead:
            return True, coords[i]


    return False, None


def check_tracks_blocked(tracks, position, direction, laserscans):
    position = position[::-1]
    #print("track.position: %s" % str(position))
    """Check if any of the lanes are blocked and return an array with the result."""
    coordinates = calculate_absolute_coordinates(position, direction, laserscans)
    results = []
    cs = []

    cxs = []
    cys = []

    #optimze: Only return results if atleast x coordinates are found.

    for t in tracks:
        progs, coords = calculate_closest_progs(t, coordinates, MAX_BLOCKER_DISTANCE_FROM_TRACK)
        blocked, coord = check_lane_blocked(t, position, progs, coords, MAX_LOOKAHEAD_DISTANCE)

        if coord is not None:
            cxs.append(coord[0])
            cys.append(coord[1])

        results.append(blocked)
        cs.append(coord)

        xs = []
        ys = []
        for x in range(int(t._circumfence)):
            p = t.get_position(float(x))
            xs.append(p[0])
            ys.append(p[1])
        # q.put((xs, ys))

    q.put((cxs, cys))
    q.put("commit")

    return results, cs
