#!/usr/bin/env python

from math import sqrt, asin, acos, atan2, pi, sin, cos, degrees
import rospy
import sys
import time
import geometry_msgs
from nav_msgs.msg import Odometry
from std_msgs.msg import Int16, UInt8
from sensor_msgs.msg import LaserScan
import signal
import track
import tf


UPPER_CIRCLE_CENTER = (215.0, 200.0)
LOWER_CIRCLE_CENTER = (215.0, 400.0)

CIRCLE_RADII = [120.0, 133.75, 161.25, 175.0]

LOOK_AHEAD_DISTANCE = -50.0 # -50 for counterclock, 50 for clockwise
STEERING_HARDNESS = 2.0

PLOT_LIST_X = []
PLOT_LIST_Y = []


class VPS: 

    def __init__(self):
        print("Initialize node.")
        self.cam_msg_sub = rospy.Subscriber("/localization/odom/3", Odometry, self.callback, queue_size=5)
        self.laser_sub = rospy.Subscriber("/scan", LaserScan, self.laser_callback, queue_size=5)
        
        self.x_coordinate = 0.0
        self.y_coordinate = 0.0
        self.z_orientation = 0.0
        self.w_orientation = 0.0

        self.drive_timeout = 0
        self.laser_scan_array_tolerance = []
        self.speed_relevant_laserscans = []

        self.blocked_tracks = []
        self.track_id = 0 # default track_id is inner line for testing purposes.
        self.tracks = [track.Track(UPPER_CIRCLE_CENTER, LOWER_CIRCLE_CENTER, r) for r in CIRCLE_RADII]

        self.current_direction = 0
        self.current_position = (0.0, 0.0)

        self.speed_pub = rospy.Publisher("/manual_control/speed", Int16, queue_size=20)
        self.steering_pub = rospy.Publisher("/steering", UInt8, queue_size=20)


    def decide_track_id(self, collision_points):
        # Decide if obstacle is close on lane and switch lanes or stop.
        
        blocked_tracks, cs = track.check_tracks_blocked(self.tracks, self.current_position, track.deg_to_vec(self.current_direction), collision_points)
        #print("CS: %s" % cs)
        #print("Blocked_tracks right after function call: %s" % str(blocked_tracks))
        self.laser_scan_array_tolerance.append(blocked_tracks)
        self.blocked_tracks = [True, True, True, True]
        for blocked in self.laser_scan_array_tolerance:
            for i in range(4):
                self.blocked_tracks[i] = self.blocked_tracks[i] and blocked[i]

        if len(self.laser_scan_array_tolerance) >= 1:
            self.laser_scan_array_tolerance.pop(0)

        if self.blocked_tracks[1] and not self.blocked_tracks[2]:
            return 2
        elif self.blocked_tracks[2] and not self.blocked_tracks[1]:
            return 1
        elif not self.blocked_tracks[1] and not self.blocked_tracks[2]:
            return 1
        else: 
            return None # Should be None


    def laser_callback(self, laserdata):
        # Pass laser coordinates from car.
        # print("Recieving laser data ...")
        laserdata = list(laserdata.ranges)
        self.drive_timeout += 1

        if self.drive_timeout > 4:
            self.drive(200, 90)

        for scan in list(enumerate(laserdata)):

            if 60<scan[0]<300 or scan[1]>1.5:
                laserdata[scan[0]] = float("inf")

            self.speed_relevant_laserscans.append(scan[1])

        self.track_id = self.decide_track_id(laserdata)
        #print(laserdata)
        #print(type(laserdata))


    def set_track_id(self, track_id):
        """Changes the defined track."""
        self.track_id = track_id


    def callback(self, data):
        self.drive_timeout = 0
        self.x_coordinate = data.pose.pose.position.x * 100.0
        self.y_coordinate = data.pose.pose.position.y * 100.0

        self.current_position = (self.x_coordinate, self.y_coordinate) # For track.py
        
        self.w_orientation = data.pose.pose.orientation.w
        self.x_orientation = data.pose.pose.orientation.x
        self.y_orientation = data.pose.pose.orientation.y
        self.z_orientation = data.pose.pose.orientation.z
        
        quaterion = (self.x_orientation, self.y_orientation, self.z_orientation, self.w_orientation)
        euler = tf.transformations.euler_from_quaternion(quaterion)
        yaw = euler[2]

        self.current_direction = degrees(yaw)
        self.current_position = track.vec_add(track.vec_mul(track.deg_to_vec(self.current_direction), 25), self.current_position)

        current_position = (self.x_coordinate, self.y_coordinate)
        print("Current position: %s" % str(current_position))

        if self.track_id == None:
            self.drive(0, 90)
            print("Blocked tracks: %s" % str(self.blocked_tracks))
            return
        print ("Track id: %s" % self.track_id)
        progress = self.tracks[self.track_id].get_progress(current_position[::-1])
        destination_point = self.tracks[self.track_id].get_position(progress + LOOK_AHEAD_DISTANCE)
        destination_point = destination_point[::-1]
        #destination_point = (300, 240) # For testing only (center of map).
        #destination_point = getClosestPoint(current_position, self.track_id, LOOK_AHEAD_DISTANCE)

        print("Blocked tracks: %s" % str(self.blocked_tracks))
        print('Destination: %s' % str(destination_point))

        angle = self.calculate_steering_angle_comfortable_point(current_position, destination_point, yaw)
        angle = degrees(-angle)
        angle *= STEERING_HARDNESS # steering hardness
        if angle<(-90):
            angle = -90
        if angle>90:
            angle = 90
        angle += 90 # add offset
        angle = int(angle)

        speed = self.decide_speed(current_position)

        print('Steering angle: %s' % angle)
        self.drive(self.decide_speed(current_position), angle)
    

    def decide_speed(self, position):
        obstacle_slowdown = 0
        closest_obstacle_distance = 150
        if self.speed_relevant_laserscans != []:
            closest_obstacle_distance = min(self.speed_relevant_laserscans)*100
            if closest_obstacle_distance == float("inf"):
                obstacle_slowdown = 0
        else:
            obstacle_slowdown = 150 - closest_obstacle_distance

        if 200<position[0]<400:
            STEERING_HARDNESS = 1.2# + (float(closest_obstacle_distance)/100*3)
            LOOK_AHEAD_DISTANCE = -50
            speed = 550 - obstacle_slowdown
            return speed
        else:
            STEERING_HARDNESS = 1.8# + (float(closest_obstacle_distance)/100*3)
            LOOK_AHEAD_DISTANCE = -30
            speed = 450 - obstacle_slowdown
            return speed


    def drive(self, speed, angle):
        if angle is None:
            angle = 90
        if self.track_id == None:
            speed = 0
        print("Setting Speed %s and Angle %s" % (speed, angle))
        self.speed_pub.publish(speed)
        self.steering_pub.publish(angle)


    def calculate_steering_angle_comfortable_point(self, current_point, destination_point, yaw):

        # Calculate these damn steering angles using arbitrary, non-norm systems.
        print("-------------------------")
        norm = track.vec_norm(track.vec_sub(destination_point, current_point))
        rad = atan2(norm[1], norm[0])

        print("rad: %s" % rad)

        print("yaw: %s" % (yaw))

        relative = rad - yaw

        print("relative: %s" % relative)

        if (-pi)<=relative<(-0.5*pi):
            relative = -0.5*pi
        elif (-0.5*pi)<=relative<0:
            pass
        elif 0<=relative<(0.5*pi):
            pass
        elif (0.5*pi)<=relative<=(pi):
            relative = 0.5*pi
        elif pi<relative:
            #print ("Maximum exceeded, countermeasures")
            relative -= 2*pi
        elif relative<(-pi):
            #print ("Maximum exceeded, countermeasures")
            relative += 2*pi
        else: 
            print ("Something went wrong :(")

        print("final relative: %s; in degrees %s:" % (relative, degrees(relative)))

        return relative


def getClosestPoint(point, laneID, distance):
    t = track.Track(UPPER_CIRCLE_CENTER, LOWER_CIRCLE_CENTER, CIRCLE_RADII[laneID])
    prog = t.get_progress(point)
    return t.get_position(prog - distance/100.0)


def main(args):
    rospy.init_node('VPS', anonymous=True)
    VPS()

    plot = False

    if plot:
        import matplotlib.pyplot as plt

        plt.ion()

        img = plt.imread("map.png")
        fig, ax = plt.subplots()

        plt.scatter([], [])
        plt.xlabel('x')
        plt.ylabel('y')

        plt.show()

    while True:

        pts = track.q.get()

        if plot:
            if pts == "commit":
                plt.show()
                plt.pause(0.001)

                plt.cla()
                ax.imshow(img)

            else:
                plt.scatter(pts[0], pts[1])

        track.q.task_done()

        #print("coordinates: %s" % zip(pts[0], pts[1]))

        if rospy.is_shutdown():
            break


if __name__ == '__main__':
    main(sys.argv)