#!/usr/bin/env python

from math import sqrt
from random import random, seed
import matplotlib.pyplot as plt
from time import time

from track import Track


UPPER_CIRCLE_CENTER = (215.0, 195.0)
LOWER_CIRCLE_CENTER = (215.0, 405.0)
CIRCLE_RADIUS = 120.0

UPPER_SEGMENT = 195.0
LOWER_SEGMENT = 405.0


def find_closest_point(x, y):

    sec = sector(x, y)

    if sec == 'upper':  # upper half-circle
        vec = (x - UPPER_CIRCLE_CENTER[0], y - UPPER_CIRCLE_CENTER[1]) # Whats our Vector, Victor?
        if vec == (0.0, 0.0):  # wow, we hit the center, just take the highest point
            vec = (0.0, -1.0)
        vec = normalize(vec)
        vec = scale(vec, CIRCLE_RADIUS)
        return add(UPPER_CIRCLE_CENTER, vec)

    elif sec == 'middle':  # middle part
        if (x < 215):  # links
            return (95, y)
        else: # rechts
            return (335, y)
    
    else:  # lower half-circle
        vec = (x - LOWER_CIRCLE_CENTER[0], y - LOWER_CIRCLE_CENTER[1]) # Whats our Vector, Victor?
        if vec == (0.0, 0.0):  # wow, we hit the center, just take the lowest point
            vec = (0.0, 1.0)
        vec = normalize(vec)
        vec = scale(vec, CIRCLE_RADIUS)
        return add(LOWER_CIRCLE_CENTER, vec)

def sector(x,y):
    if y <= UPPER_SEGMENT:
        return 'upper'
    elif y < LOWER_SEGMENT:
        return 'middle'
    return 'lower'


def normalize(vec):
    l = sqrt(vec[0]**2 + vec[1]**2)
    return (vec[0]/l, vec[1]/l)


def scale(vec, factor):
    return (vec[0]*factor, vec[1]*factor)


def add(vec1, vec2):
    return (vec1[0]+vec2[0], vec1[1]+vec2[1])    


def test():

    xs = []
    ys = []

    with open("coords.txt" , "r") as f:
        for l in f:
            y, x = l.split(",")
            xs.append(float(x))
            ys.append(float(y))

    seed(time())

    # for n in range(0, 20):
    #     array.append((random() * 430, random() * 600))

    t = Track(UPPER_CIRCLE_CENTER, LOWER_CIRCLE_CENTER, CIRCLE_RADIUS)

    img = plt.imread("map.png")
    fig, ax = plt.subplots()
    ax.imshow(img)

    plt.scatter(xs, ys)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

test()
